FROM python:3.7

RUN pip install websockets
COPY server.py /

CMD python server.py
